<?php
/**
 * @file
 * erpal_task_templates.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function erpal_task_templates_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_task_template_settings'
  $field_bases['field_task_template_settings'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_task_template_settings',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'template' => 'Use as template',
        'default_template' => 'Clone as default task in every project',
        'default_template_children' => 'Clone as default task in every project WITH child tasks',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
