<?php

/**
* @file functions for book actions (bulk operations) to clone from books
*/

/**
* Action callback to clone book page
*/
function erpal_task_templates_clone_task_action(&$tasks, &$context) {
  $task_nids = array_keys($tasks);
  $parent_nid = $context['parent_nid'];
  
  //build options
  $options = array();
  $options['clone_children'] = !empty($context['clone_children']) ? $context['clone_children'] : 0;

  //start cloning batch operation
  _erpal_task_templates_clone_tasks_batch($task_nids, $parent_nid, $options);
}

/**
* Callback for configurable form of book clone bulk operation
*/
function erpal_task_templates_clone_task_action_form($context, $form_state) {
  $parent_nid = $context['view']->args[0];
  $parent_node = node_load($parent_nid);  //this is the parent 

  if ($parent_node->type != 'erpal_task' && $parent_node->type != 'erpal_project') {
    //we clone only book pages
    drupal_set_message(t('You can only clone to projects or tasks.'), 'error');
    drupal_goto('node/'.$parent_nid);
  }
 
  //now build the settings form for configurable bulk operation
  $form = array();
  $form['clone_children'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clone children'),
    '#default_value' => true,
    '#weight' => -15,
    '#description' => t('If checked, all children of the selected templates will be cloned, too'),
  );
  
  //add the parent node and the book node where all cloned nodes will be assigned to
  $form['parent_nid'] = array(
    '#type' => 'value',
    '#value' => $parent_node->nid,
  );
  
  if ($parent_node->type == 'erpal_project')
	$project_nid = $parent_node->nid;
  if ($parent_node->type == 'erpal_task')
	$project_nid = $parent_node->field_project_ref[LANGUAGE_NONE][0]['target_id'];	
	
  $form['project_nid'] = array(
    '#type' => 'value',
    '#value' => $project_nid,
  );
  
  return $form;
}

function erpal_task_templates_clone_task_action_validate($form, $form_state) {
  module_invoke_all('erpal_clone_task_page_validate', $form, $form_state);
}

/**
* Submit handler for book page clone bulk operation
*/
function erpal_task_templates_clone_task_action_submit($form, &$form_state) {
  $values = $form_state['values'];
  
  return $values;
}