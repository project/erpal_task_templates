<?php

/**
* Provide functions to clone tasks to projects or tasks
*/

/**
* Clones all task nids including all children in deep search with a batch process
* @param $task_nids an array of nids of tasks to clone
* @param $top_parent_nid all new cloned tasks at top most level will be assigned to this parent
*/
function _erpal_task_templates_clone_tasks_batch($task_nids, $top_parent_nid, $options=array()) {

  $batch = array(
    'operations' => array(
      array('_erpal_task_templates_clone_tasks_batch_operation', array($task_nids, $top_parent_nid, $options)),
      ),
    'finished' => '_erpal_task_templates_clone_tasks_finished',
    'title' => t('Processing Clone Batch'),
    'init_message' => t('Clone Batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Clone Batch has encountered an error.'),
  );
  batch_set($batch);

  batch_process('node/'.$top_parent_nid); //return to the parent
}

/**
* @TODO: This is almost the same as in book.clone.inc file of erpal_book_helper module. We should abstrct this!
* Clones all given nodes
* @param $task_nids an array of nids of book_pages to clone
* @param $parent_nid all new cloned book pages will be assigned to this parent
*/
function _erpal_task_templates_clone_tasks_batch_operation($task_nids, $parent_nid, $options=array(), &$context) {  
  if (!isset($context['sandbox']['progress'])) {
    //get all children to clone
    $all_children = array();
    $total = 0;
    foreach ($task_nids as $task_nid) {
      $types = isset($options['types']) ? $options['types'] : false;
      if (!$types)
        $types = array('erpal_task');

      //clone children?
      if ($options['clone_children'])
        $new_children = _erpal_basic_helper_get_all_child_nids_deep_search($task_nid, $types);
      else
        $new_children = array();
        
      $top_node = array('parent' => $parent_nid, 'child' => $task_nid);
      $all_children[$task_nid] = array_merge(array($top_node), $new_children);
            
      $total += count($all_children[$task_nid]);
    }

    //add the project, all book_pages will be added to
    $parent_node = node_load($parent_nid);
    if ($parent_node->type == 'erpal_project')
      $project_nid = $parent_node->nid;
    elseif ($parent_node->type == 'erpal_task')
      $project_nid = $parent_node->field_project_ref[LANGUAGE_NONE][0]['target_id'];
    
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['project_nid'] = $project_nid;  //the project_nid where all book_pages will be added to
    $context['sandbox']['current_delta'] = -1;  //the current delta in queue of all children
    $context['sandbox']['current_parent'] = 0; //the current parent_nid of all task_nids
    $context['sandbox']['all_children'] = $all_children;
    $context['sandbox']['max'] = $total;
  }
  
  //only process one node at a time
  $all_children = $context['sandbox']['all_children'];
  $project_nid = $context['sandbox']['project_nid'];
  $clone_parent_nid = $context['sandbox']['current_parent'];
  //if no parent is set, take the first for our flat childrens array
  if (!$clone_parent_nid) {
    $all_parents = array_keys($all_children);
    $clone_parent_nid = $all_parents[0];
  }
   
  $delta = $context['sandbox']['current_delta'];
  
  $children = $all_children[$clone_parent_nid];
  $child = ($delta > -1) ? $children[$delta] : $children[0];

  //if we process the first delta, of a tree parent, we use the given function argument parent_nid to add the new top level
  //children.
  if ($delta > -1) {
    $parent_nid = $child['parent'];    
  } else {
    $delta = 0;
  }
  //otherwise keep the value from function argument
  
  $old_child_nid = $child['child'];
  // Here we actually perform our processing on the current node.
  $new_node = _erpal_task_templates_clone_node($old_child_nid, $parent_nid, $project_nid, $options);

  //now change the parent of all child nodes to new nid if parent_nid is set
  foreach ($all_children as $a_parent_nid=>$a_children) {
    foreach ($all_children[$a_parent_nid] as $a_delta=>$a_child) {
      $a_parent = $a_child['parent'];
      if ($a_parent == $old_child_nid) {
        //set parent_nid to new cloned node
        $all_children[$a_parent_nid][$a_delta]['parent'] = $new_node->nid;
      }
    }
  }
  
  //set new delta and parent_nid values
  //check if there is a next delta
  
  if (isset($all_children[$clone_parent_nid][$delta +1])) {
    //use this one for the next step
    $delta = $delta +1;    
  } else {
    //otherwise stept to next parent if there is one to clone
    $parents = array_keys($all_children);
    $current_index = array_search($clone_parent_nid, $parents);
    $next_index = $current_index +1;

    if (!isset($parents[$next_index]))
      $clone_parent_nid = 0;
    else
      $clone_parent_nid = $parents[$next_index];
      
    $delta = -1;  //reset delta
  }
  
  // Store some result for post-processing in the finished callback.
  $context['results'][] = check_plain($new_node->title);

  //set the new children with updated parent nids
  $context['sandbox']['all_children'] = $all_children;
  // Update our progress information.
  $context['sandbox']['progress']++;
  $context['sandbox']['current_delta'] = $delta;
  $context['sandbox']['current_parent'] = $clone_parent_nid;

  if (!$clone_parent_nid) {
    $context['finished'] = 1; //finsihed because there is no other parent to process
  } else {
    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    } else
      $context['finished'] = 1;
  }
  
  $context['message'] = t('Now processing %node', array('%node' => $node->title));
}

/**
* Callback of erpal book clone batch process finished
*/
function _erpal_task_templates_clone_tasks_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = count($results) .' processed.';
    $message .= theme('item_list', $results);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
  }
  drupal_set_message($message);
}

/**
* Clones a simple node and saves it
*/
function _erpal_task_templates_clone_node($nid, $parent_nid, $project_nid, $options) {

  if (is_numeric($nid)) {
    global $user;

    $node = node_load($nid);
    if (isset($node->nid)) {
      $original_node = clone $node; //clone the total node object!

      //alter some variables of this node
      $node->nid = NULL;
      $node->vid = NULL;
      $node->tnid = NULL;
      $node->name = $user->name;
      $node->uid = $user->uid;
      $node->created = NULL;
      $node->menu = NULL;
      $node->path = NULL;
      $node->files = array();
    
      //unset the template flag because if a book was a template, the cloned one should not be a template
      unset($node->field_task_template_settings);
    
      //unset other fields
      unset($node->field_task_assigned_users);
      unset($node->field_date);
      unset($node->field_estimated_time);
      unset($node->field_pricing);
	
      if ($project_nid)
        $node->field_project_ref[LANGUAGE_NONE][0]['target_id'] = $project_nid;

      //set the parent
      if ($parent_nid != $project_nid) { //in case that the parent is the project itself, we will not assign the project as parent, cause it is already assigned by field_project_ref
        $node->field_parent[LANGUAGE_NONE][0]['target_id'] = $parent_nid;
      }
      $title = isset($options['title']) ? $options['title'] : $original_node->title;
      if ($title)
        $node->title = $title;
     
      //Hook to let other modules clone children or set some properties
      $args = array('project_nid' => $project_nid, 'parent_nid' => $parent_nid, 'original_node' => $original_node, 'options' => $options, 'original_node' => $original_node);
     
      drupal_alter('erpal_clone_task_node', $node, $args);

      node_save($node);
      //ensure that the project reference is set properly. Sometimes this wasn't and I dont know why, so set it again and just save the field
      $node->field_project_ref[LANGUAGE_NONE][0]['target_id'] = $project_nid;
      field_attach_update('node', $node);
      
      drupal_alter('erpal_clone_task_node_postprocess', $node, $args);

      return $node;
      
    } else {
      return false;
    }
    
  }
}

