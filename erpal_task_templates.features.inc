<?php
/**
 * @file
 * erpal_task_templates.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function erpal_task_templates_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "cat" && $api == "cat") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function erpal_task_templates_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_ds_layout_settings_info_alter().
 */
function erpal_task_templates_ds_layout_settings_info_alter(&$data) {
  if (isset($data['node|erpal_task|form'])) {
    $data['node|erpal_task|form']->settings['fields']['field_task_template_settings'] = 'advancedleft'; /* WAS: '' */
    $data['node|erpal_task|form']->settings['regions']['advancedleft']['7'] = 'field_task_template_settings'; /* WAS: '' */
  }
}
