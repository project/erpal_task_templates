<?php
/**
 * @file
 * erpal_task_templates.cat.inc
 */

/**
 * Implements hook_cat_items_settings_info().
 */
function erpal_task_templates_cat_items_settings_info() {
  $export = array();

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_project_task_templates';
  $cat_item->name = 'Erpal project: task templates';
  $cat_item->category = 'project';
  $cat_item->path = 'projects/tasks/templates';
  $cat_item->data = array(
    'access' => array(),
    'active_trail_path' => 'projects/projects',
    'arguments' => array(),
    'name' => 'Erpal project: task templates',
    'path' => 'projects/tasks/templates',
    'category' => 'project',
    'cat_actions' => array(),
    'cat_tabs' => array(
      0 => array(
        'path' => 'projects/projects',
        'path_router' => 'projects/projects',
        'path_original' => 'projects/projects',
        'path_pattern' => 'projects/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Project list',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_projects|project_list',
      ),
      1 => array(
        'path' => 'projects/timetrackings/tmp',
        'path_router' => 'projects/timetrackings/tmp',
        'path_original' => 'projects/timetrackings/tmp',
        'path_pattern' => 'projects/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Active timetrackings',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_projects|active_timetrackings',
      ),
      2 => array(
        'path' => 'projects/timetrackings',
        'path_router' => 'projects/timetrackings',
        'path_original' => 'projects/timetrackings',
        'path_pattern' => 'projects/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Timetrackings',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_projects|timetrackings',
      ),
      3 => array(
        'path' => 'projects/pricing/fixed',
        'path_router' => 'projects/pricing/fixed',
        'path_original' => 'projects/pricing/fixed',
        'path_pattern' => 'projects/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Fixed price items',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_projects|pricing_fixed',
      ),
      4 => array(
        'path' => 'projects/budgets',
        'path_router' => 'projects/budgets',
        'path_original' => 'projects/budgets',
        'path_pattern' => 'projects/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Time-Budgets',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_projects|budgets',
      ),
      5 => array(
        'path' => 'projects/tasks/templates',
        'path_router' => 'projects/tasks/templates',
        'path_original' => 'projects/tasks/templates',
        'path_pattern' => 'projects/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Task templates',
        'css_id' => '',
        'css_class' => 'tasks',
        'active' => TRUE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_projects_task_template|task_templates',
      ),
    ),
    'machine_name' => 'erpal_project_task_templates',
    'path_original' => 'projects/tasks/templates',
    'path_pattern' => 'projects/%/%',
    'path_query' => array(),
    'path_router' => 'projects/tasks/templates',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_project_task_templates'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_project_task_templates_clone';
  $cat_item->name = 'Erpal project: task templates clone to task';
  $cat_item->category = 'Project';
  $cat_item->path = 'projects/tasks/templates/clone/%';
  $cat_item->data = array(
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'erpal_task' => 'erpal_task',
            ),
          ),
          'context' => 'node',
          'not' => FALSE,
        ),
      ),
      'logic' => 'or',
    ),
    'active_trail_path' => 'projects/projects',
    'arguments' => array(
      4 => array(
        'argument_name' => 'node',
        'argument_type' => 'path',
        'position' => 4,
        'argument' => 'entity_id:node',
        'settings' => array(
          'identifier' => 'Node: ID',
        ),
      ),
    ),
    'name' => 'Erpal project: task templates clone to task',
    'path' => 'projects/tasks/templates/clone/%',
    'category' => 'Project',
    'cat_actions' => array(),
    'cat_tabs' => array(
      0 => array(
        'path' => 'node/[node:field-project-ref:nid]',
        'path_router' => 'node/[node:field-project-ref:nid]',
        'path_original' => 'node/[node:field-project-ref:nid]',
        'path_pattern' => 'node/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Summary',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project_tasks_view|summary',
      ),
      1 => array(
        'path' => 'node/[node:field-project-ref:nid]/tasks',
        'path_router' => 'node/[node:field-project-ref:nid]/tasks',
        'path_original' => 'node/[node:field-project-ref:nid]/tasks',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Tasks',
        'css_id' => '',
        'css_class' => '',
        'active' => TRUE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project_tasks_view|tasks',
      ),
      2 => array(
        'path' => 'node/[node:field-project-ref:nid]/tickets',
        'path_router' => 'node/[node:field-project-ref:nid]/tickets',
        'path_original' => 'node/[node:field-project-ref:nid]/tickets',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Tickets',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project_tasks_view|tickets',
      ),
      3 => array(
        'path' => 'node/[node:field-project-ref:nid]/files',
        'path_router' => 'node/[node:field-project-ref:nid]/files',
        'path_original' => 'node/[node:field-project-ref:nid]/files',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Files',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project_tasks_view|files',
      ),
      4 => array(
        'path' => 'node/[node:field-project-ref:nid]/calendar',
        'path_router' => 'node/[node:field-project-ref:nid]/calendar',
        'path_original' => 'node/[node:field-project-ref:nid]/calendar',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Calendar',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project_tasks_view|calendar',
      ),
      5 => array(
        'path' => 'node/[node:field-project-ref:nid]/timetracking',
        'path_router' => 'node/[node:field-project-ref:nid]/timetracking',
        'path_original' => 'node/[node:field-project-ref:nid]/timetracking',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Timetrackings',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project_tasks_view|timetrackings',
      ),
      6 => array(
        'path' => 'node/[node:field-project-ref:nid]/releases',
        'path_router' => 'node/[node:field-project-ref:nid]/releases',
        'path_original' => 'node/[node:field-project-ref:nid]/releases',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Releases',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project_tasks_view|releases',
      ),
    ),
    'machine_name' => 'erpal_project_task_templates_clone',
    'path_original' => 'projects/tasks/templates/clone/%node',
    'path_pattern' => 'projects/%/%/%/%',
    'path_query' => array(),
    'path_router' => 'projects/tasks/templates/clone/%',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_project_task_templates_clone'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_project_task_templates_clone_to_project';
  $cat_item->name = 'Erpal project: task templates clone to project';
  $cat_item->category = 'Project';
  $cat_item->path = 'projects/tasks/templates/clone/%';
  $cat_item->data = array(
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'erpal_project' => 'erpal_project',
            ),
          ),
          'context' => 'node',
          'not' => FALSE,
        ),
      ),
      'logic' => 'or',
    ),
    'active_trail_path' => 'projects/projects',
    'arguments' => array(
      4 => array(
        'argument_name' => 'erpal_project',
        'argument_type' => 'path',
        'position' => 4,
        'argument' => 'entity_id:node',
        'settings' => array(
          'identifier' => 'Node: ID',
        ),
      ),
    ),
    'name' => 'Erpal project: task templates clone to project',
    'path' => 'projects/tasks/templates/clone/%',
    'category' => 'Project',
    'cat_actions' => array(),
    'cat_tabs' => array(
      0 => array(
        'path' => 'node/[erpal_project:nid]',
        'path_router' => 'node/[erpal_project:nid]',
        'path_original' => 'node/[erpal_project:nid]',
        'path_pattern' => 'node/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Summary',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project|summary',
      ),
      1 => array(
        'path' => 'node/[erpal_project:nid]/tasks',
        'path_router' => 'node/[erpal_project:nid]/tasks',
        'path_original' => 'node/[erpal_project:nid]/tasks',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Tasks',
        'css_id' => '',
        'css_class' => '',
        'active' => TRUE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project|tasks',
      ),
      2 => array(
        'path' => 'node/[erpal_project:nid]/tickets',
        'path_router' => 'node/[erpal_project:nid]/tickets',
        'path_original' => 'node/[erpal_project:nid]/tickets',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Tickets',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project|tickets',
      ),
      3 => array(
        'path' => 'node/[erpal_project:nid]/files',
        'path_router' => 'node/[erpal_project:nid]/files',
        'path_original' => 'node/[erpal_project:nid]/files',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Files',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project|files',
      ),
      4 => array(
        'path' => 'node/[erpal_project:nid]/calendar',
        'path_router' => 'node/[erpal_project:nid]/calendar',
        'path_original' => 'node/[erpal_project:nid]/calendar',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Calendar',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project|calendar',
      ),
      5 => array(
        'path' => 'node/[erpal_project:nid]/timetracking',
        'path_router' => 'node/[erpal_project:nid]/timetracking',
        'path_original' => 'node/[erpal_project:nid]/timetracking',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Timetrackings',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project|timetrackings',
      ),
      6 => array(
        'path' => 'node/[erpal_project:nid]/releases',
        'path_router' => 'node/[erpal_project:nid]/releases',
        'path_original' => 'node/[erpal_project:nid]/releases',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Releases',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_project|releases',
      ),
    ),
    'machine_name' => 'erpal_project_task_templates_clone_to_project',
    'path_original' => 'projects/tasks/templates/clone/%erpal_project',
    'path_pattern' => 'projects/%/%/%/%',
    'path_query' => array(),
    'path_router' => 'projects/tasks/templates/clone/%',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_project_task_templates_clone_to_project'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_project_tasks_clone_tasks';
  $cat_item->name = 'Erpal project tasks: clone tasks from task list';
  $cat_item->category = 'Project';
  $cat_item->path = 'node/%node/tasks';
  $cat_item->data = array(
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'erpal_project' => 'erpal_project',
              'erpal_task' => 'erpal_task',
            ),
          ),
          'context' => 'node',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'active_trail_path' => '',
    'arguments' => array(
      1 => array(
        'argument_name' => 'node',
        'argument_type' => 'path',
        'position' => 1,
        'argument' => 'entity_id:node',
        'settings' => array(
          'identifier' => 'Node: ID',
        ),
      ),
    ),
    'name' => 'Erpal project tasks: clone tasks from task list',
    'path' => 'node/%node/tasks',
    'category' => 'Project',
    'cat_actions' => array(
      0 => array(
        'path' => 'projects/tasks/templates/clone/[node:nid]',
        'path_router' => 'projects/tasks/templates/clone/[node:nid]',
        'path_original' => 'projects/tasks/templates/clone/[node:nid]',
        'path_pattern' => 'projects/%/%/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Add cloned tasks',
        'css_id' => '',
        'css_class' => '',
        'active' => TRUE,
      ),
    ),
    'cat_tabs' => array(),
    'machine_name' => 'erpal_project_tasks_clone_tasks',
    'path_original' => 'node/%node/tasks',
    'path_pattern' => 'node/%/%',
    'path_query' => array(),
    'path_router' => 'node/%/tasks',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_project_tasks_clone_tasks'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_projects_budgets_task_templates';
  $cat_item->name = 'Erpal projects budgets: Task templates';
  $cat_item->category = 'Project';
  $cat_item->path = 'projects/budgets';
  $cat_item->data = array(
    'access' => array(),
    'active_trail_path' => '',
    'arguments' => array(),
    'name' => 'Erpal projects budgets: Task templates',
    'path' => 'projects/budgets',
    'category' => 'Project',
    'cat_actions' => array(),
    'cat_tabs' => array(
      0 => array(
        'path' => 'projects/tasks/templates',
        'path_router' => 'projects/tasks/templates',
        'path_original' => 'projects/tasks/templates',
        'path_pattern' => 'projects/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Task templates',
        'css_id' => '',
        'css_class' => 'tasks',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_projects_task_template|task_templates',
      ),
    ),
    'machine_name' => 'erpal_projects_budgets_task_templates',
    'path_original' => 'projects/budgets',
    'path_pattern' => 'projects/%',
    'path_query' => array(),
    'path_router' => 'projects/budgets',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_projects_budgets_task_templates'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_projects_fixed_price_task_templates';
  $cat_item->name = 'Erpal projects fixed price: task templates';
  $cat_item->category = 'projects';
  $cat_item->path = 'projects/pricing/fixed';
  $cat_item->data = array(
    'access' => array(),
    'active_trail_path' => '',
    'arguments' => array(),
    'name' => 'Erpal projects fixed price: task templates',
    'path' => 'projects/pricing/fixed',
    'category' => 'projects',
    'cat_actions' => array(),
    'cat_tabs' => array(
      0 => array(
        'path' => 'projects/tasks/templates',
        'path_router' => 'projects/tasks/templates',
        'path_original' => 'projects/tasks/templates',
        'path_pattern' => 'projects/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Task templates',
        'css_id' => '',
        'css_class' => 'tasks',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_projects_task_template|task_templates',
      ),
    ),
    'machine_name' => 'erpal_projects_fixed_price_task_templates',
    'path_original' => 'projects/pricing/fixed',
    'path_pattern' => 'projects/%/%',
    'path_query' => array(),
    'path_router' => 'projects/pricing/fixed',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_projects_fixed_price_task_templates'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_projects_task_template';
  $cat_item->name = 'Erpal projects: task template';
  $cat_item->category = 'Project';
  $cat_item->path = 'projects/projects';
  $cat_item->data = array(
    'access' => array(),
    'active_trail_path' => '',
    'arguments' => array(),
    'name' => 'Erpal projects: task template',
    'path' => 'projects/projects',
    'category' => 'Project',
    'cat_actions' => array(),
    'cat_tabs' => array(
      0 => array(
        'path' => 'projects/tasks/templates',
        'path_router' => 'projects/tasks/templates',
        'path_original' => 'projects/tasks/templates',
        'path_pattern' => 'projects/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Task templates',
        'css_id' => '',
        'css_class' => 'tasks',
        'active' => FALSE,
        'reusable' => TRUE,
        'machine_name' => 'task_templates',
        'reusabled' => FALSE,
        'reusabled_machine_name' => NULL,
      ),
    ),
    'machine_name' => 'erpal_projects_task_template',
    'path_original' => 'projects/projects',
    'path_pattern' => 'projects/%',
    'path_query' => array(),
    'path_router' => 'projects/projects',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_projects_task_template'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_projects_timetrackings_final_task_templates';
  $cat_item->name = 'Erpal projects timetrackings final: task templates';
  $cat_item->category = 'projects';
  $cat_item->path = 'projects/timetrackings';
  $cat_item->data = array(
    'access' => array(),
    'active_trail_path' => '',
    'arguments' => array(),
    'name' => 'Erpal projects timetrackings final: task templates',
    'path' => 'projects/timetrackings',
    'category' => 'projects',
    'cat_actions' => array(),
    'cat_tabs' => array(
      0 => array(
        'path' => 'projects/tasks/templates',
        'path_router' => 'projects/tasks/templates',
        'path_original' => 'projects/tasks/templates',
        'path_pattern' => 'projects/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Task templates',
        'css_id' => '',
        'css_class' => 'tasks',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_projects_task_template|task_templates',
      ),
    ),
    'machine_name' => 'erpal_projects_timetrackings_final_task_templates',
    'path_original' => 'projects/timetrackings',
    'path_pattern' => 'projects/%',
    'path_query' => array(),
    'path_router' => 'projects/timetrackings',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_projects_timetrackings_final_task_templates'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_projects_timtrackings_task_templates';
  $cat_item->name = 'Erpal projects timtrackings tmp: task templates';
  $cat_item->category = 'projects';
  $cat_item->path = 'projects/timetrackings/tmp';
  $cat_item->data = array(
    'access' => array(),
    'active_trail_path' => '',
    'arguments' => array(),
    'name' => 'Erpal projects timtrackings tmp: task templates',
    'path' => 'projects/timetrackings/tmp',
    'category' => 'projects',
    'cat_actions' => array(),
    'cat_tabs' => array(
      0 => array(
        'path' => 'projects/tasks/templates',
        'path_router' => 'projects/tasks/templates',
        'path_original' => 'projects/tasks/templates',
        'path_pattern' => 'projects/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Task templates',
        'css_id' => '',
        'css_class' => 'tasks',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_projects_task_template|task_templates',
      ),
    ),
    'machine_name' => 'erpal_projects_timtrackings_task_templates',
    'path_original' => 'projects/timetrackings/tmp',
    'path_pattern' => 'projects/%/%',
    'path_query' => array(),
    'path_router' => 'projects/timetrackings/tmp',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_projects_timtrackings_task_templates'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_task_templates_clone_tasks_from_task_view';
  $cat_item->name = 'Erpal task templates: clone tasks from task view';
  $cat_item->category = 'Project';
  $cat_item->path = 'node/%';
  $cat_item->data = array(
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'erpal_task' => 'erpal_task',
            ),
          ),
          'context' => 'node',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'active_trail_path' => '',
    'arguments' => array(
      1 => array(
        'argument_name' => 'node',
        'argument_type' => 'path',
        'position' => 1,
        'argument' => 'entity_id:node',
        'settings' => array(
          'identifier' => 'Node: ID',
        ),
      ),
    ),
    'name' => 'Erpal task templates: clone tasks from task view',
    'path' => 'node/%',
    'category' => 'Project',
    'cat_actions' => array(
      0 => array(
        'path' => 'projects/tasks/templates/clone/[node:nid]',
        'path_router' => 'projects/tasks/templates/clone/[node:nid]',
        'path_original' => 'projects/tasks/templates/clone/[node:nid]',
        'path_pattern' => 'projects/%/%/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Add cloned tasks',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
      ),
    ),
    'cat_tabs' => array(),
    'machine_name' => 'erpal_task_templates_clone_tasks_from_task_view',
    'path_original' => 'node/%node',
    'path_pattern' => 'node/%',
    'path_query' => array(),
    'path_router' => 'node/%',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_task_templates_clone_tasks_from_task_view'] = $cat_item;

  return $export;
}
