<?php
/**
 * @file
 * erpal_task_templates.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function erpal_task_templates_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: ds_layout_settings
  $overrides["ds_layout_settings.node|erpal_task|form.settings|fields|field_task_template_settings"] = 'advancedleft';
  $overrides["ds_layout_settings.node|erpal_task|form.settings|regions|advancedleft|7"] = 'field_task_template_settings';

 return $overrides;
}
